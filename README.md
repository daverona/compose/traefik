# daverona/compose/traefik

## Prerequisites

* `traefik.example` mapped to `127.0.0.1` &mdash; check [this](https://gitlab.com/daverona/compose/dnsmasq) out

## Quick Start

```bash
cp .env.example .env
# edit .env
docker compose up --detach
```

Visit [http://traefik.example](http://traefik.example).

## References

* Traefik Documentation: [https://docs.traefik.io/](https://docs.traefik.io/)
* Traefik CLI References: [https://doc.traefik.io/traefik/reference/static-configuration/cli-ref/](https://doc.traefik.io/traefik/reference/static-configuration/cli-ref/)
* Traefik repository: [https://github.com/containous/traefik](https://github.com/containous/traefik)
* Traefik registry: [https://hub.docker.com/\_/traefik](https://hub.docker.com/_/traefik)
* [https://medium.com/faun/limit-clients-to-specific-ips-with-treaefik-v2-ipwhitelist-middlewares-24357cf8f6ba](https://medium.com/faun/limit-clients-to-specific-ips-with-treaefik-v2-ipwhitelist-middlewares-24357cf8f6ba)
